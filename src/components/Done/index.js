import React from "react";
import classes from './index.module.sass'

const birthDate = () => {
    const birth = new Date(2015,9,18)
    return parseInt((new Date().getTime() - birth.getTime())/ 24 / 3600 / 1000)
}

const Done = () => {

    return (
        <div className={classes.container} id={'feedback'}>
            <h1>ЧТО УЖЕ СДЕЛАНО?</h1>
            <div className={classes.container_content}>
                <div className={classes.container_content_left}>
                    <p>>279000</p>
                    <p>3623</p>
                    <p>{birthDate()}</p>
                </div>
                <div className={classes.container_content_line}></div>
                <div className={classes.container_content_right}>
                    <p>кв.м. убрано нашими работниками</p>
                    <p>клиента</p>
                    <p>дней на рынке</p>
                </div>
            </div>
        </div>
    )
}

export default Done
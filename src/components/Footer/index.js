import React from "react";
import classes from "./index.module.sass";
import {ReactComponent as Logo} from "../../assets/Logo_footer.svg";
import {ReactComponent as FaceBook} from "../../assets/fb.svg";
import {ReactComponent as Instagram} from "../../assets/insta.svg";

const Footer = () => {

    return (
        <div className={classes.container} id={'contacts'}>
            <div className={classes.container_content}>
                <div className={classes.container_content_logo}>
                    <Logo/>
                </div>
                <div className={classes.container_content_social}>
                    <h1>social media</h1>
                    <div className={classes.container_content_social_logos}>
                        <FaceBook/>
                        <Instagram/>
                    </div>
                </div>
                <div className={classes.container_content_contacts}>
                    <h1>contacts</h1>
                    <div className={classes.container_content_contacts_text}>
                        <p><b>адрес</b>: ул. Ленсовета, д. 23, офис 36</p>
                        <p><b>телефон</b>: 8 911 419 22 77</p>
                        <p><b>e-mail</b>: tsosich@clean.app</p>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default Footer
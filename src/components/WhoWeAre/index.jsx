import React from "react";
import classes from './index.module.sass'
import GreenRectangle from "../../hoc/GreenRectangle";

const WhoWeAre = () => {

    return (
        <GreenRectangle id={'about'}>
            <div className={classes.container}>
                <h1>КТО МЫ?</h1>
                <div className={classes.container_content}>
                    <div className={classes.container_content_left}>
                        <p>у нас прекрасная репутация, и мы уже успели завоевать доверие и расположение своих клиентов</p>
                        <p>мы регулярно измеряем температуру работников и работаем в масках</p>
                    </div>
                    <div className={classes.container_content_line}></div>
                    <div className={classes.container_content_right}>
                        <p>наша компания оказывает полный спектр клининговых услуг</p>
                        <p>оплата картой, сдача ключей в офис, управление заказами в приложении</p>
                    </div>
                </div>
            </div>
        </GreenRectangle>
    )
}

export default WhoWeAre
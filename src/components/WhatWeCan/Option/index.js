import React from "react";
import classes from './index.module.sass'
import {ReactComponent as Plus} from "../../../assets/plus.svg";

const Option = ({title, text, price, setValue, setNext}) => {

    return (
        <div className={classes.container}>
            <div className={classes.container_plus} onClick={()=>{setValue('type',title);setValue('price',price);setNext()}}>
                <Plus/>
            </div>
            <div className={classes.container_text}>
                <h1>{title}</h1>
                <p>{text}</p>
                <h2>от {price}₽</h2>
            </div>
        </div>
    )
}

export default Option
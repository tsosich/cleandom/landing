import React, {useEffect, useState} from 'react'
import classes from './index.module.sass'
import Option from "./option";
import {ReactComponent as Close} from "../../../assets/close.svg";

const initOptions = [
    {name: 'Площадь', key: 'square', amount: 0, price: 30, id: 'square123', step:10, max: 10000},
    {name: 'Окна', key: 'window', amount: 0, price: 300, id: 'window123', step: 1, max: 1000},
    {name: 'Санузел', key: 'toilet', amount: 0, price: 300, id: 'toilet123', step: 1, max: 1000},
    {name: 'Химчистка ковров', key: 'carpet', amount: 0, price: 300, id: 'chem12345', step: 1, max: 1000},
]

const FormOrder = ({close, title, setValue, price, setNext}) => {

    const [options, setOptions] = useState(initOptions)
    const [finalPrice, setFinalPrice] = useState(price)

    useEffect(()=>{
        let sum = price
        options.forEach(option => sum += option.price * option.amount)
        setFinalPrice(sum)
    },[options])

    const plusHandler = id => {
        setOptions(options.map(option => option.id === id ? {
            ...option,
            amount: option.amount + option.step > option.max ? option.amount : option.amount + option.step
        } : option))
    }

    const minusHandler = id => {
        setOptions(options.map(option => option.id === id ? {
            ...option,
            amount: ((option.amount - option.step) > 0 ? option.amount - option.step : 0)
        } : option))
    }

    const submitHandler = () => {
        options.forEach(option => setValue(option.key, option.amount))
        setValue('price',finalPrice)
        setNext()
    }

    return (
        <div className={classes.container}>
            <div className={classes.container_close} onClick={close}>
                <Close/>
            </div>
            <div className={classes.container_title}>
                <p>{title}</p>
            </div>
            <div className={classes.container_options}>
                {options.map(option => (
                    <Option key={option.id} id={option.id} name={option.name} amount={option.amount}
                            price={option.price} plusHandler={plusHandler} minusHandler={minusHandler}/>
                ))}
            </div>

            <div className={classes.container_footer}>
                <div className={classes.container_footer_price}>
                    <p>Стоимость: <span>{finalPrice}₽</span></p>
                </div>
                <div className={classes.container_footer_registration} onClick={() => submitHandler()}>
                    <p>Перейти к оформлению</p>
                </div>
            </div>
        </div>
    )
}

export default FormOrder
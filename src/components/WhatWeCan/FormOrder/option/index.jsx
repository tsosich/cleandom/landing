import React from 'react'
import classes from "./index.module.sass";

const Option = ({id, name, amount, price, plusHandler, minusHandler}) => {
    return (
        <div className={classes.container}>
            <p>{name}</p>
            {price > 0 &&
            <div className={classes.container_price}>
                <p>{price}₽ x</p>
            </div>
            }
            <div className={classes.container_control}>
                <img src='icons/minus.svg' onClick={() => minusHandler(id)}/>
                <p>{amount}</p>
                <img src='icons/plus.svg' onClick={() => plusHandler(id)}/>
            </div>
        </div>
    )
}

export default Option
import React from "react";
import classes from './index.module.sass'

const Input = ({placeholder, value, type, Icon}) => {
    return (
        <div className={classes.container}>
            <input
                type={type}
                placeholder={placeholder}
                value={value}
            />
            <div className={classes.container_icon}>
                <Icon/>
            </div>
        </div>
    )
}

export default Input
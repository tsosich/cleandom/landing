import React, {useState} from "react";
import classes from './index.module.sass'
import Input from "./Input";
import InputMask from 'react-input-mask'
import {ReactComponent as Date} from "../../../assets/date_icon.svg";
import MaskedInput from "react-text-mask/dist/reactTextMask";
import {useFormik} from "formik";
import {ReactComponent as Next} from "../../../assets/arrow_next.svg";
import {ReactComponent as Prev} from "../../../assets/arrow_prev.svg";
import {ReactComponent as Close} from "../../../assets/close.svg";
import clsx from "clsx";

const masks = {
    phone: /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/,
    date: /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/,
    time: /^(0[0-9]|1[0-9]|2[0-3]|[0-9]):[0-5][0-9]$/
}

const validate = values => {
    const errors = {}

    if (!values.name)
        errors.name = 'пидорас'
    if(!values.phone)
        errors.phone = 'пидорас'
    if(!values.date){
        errors.date = 'пидорас'
    } else if(!masks.date.exec(values.date)){
        errors.date = 'пидорас'
    }
    if(!values.time){
        errors.time = 'пидорас'
    } else if(!masks.time.exec(values.time)){
        errors.time = 'пидорас'
    }

    if(!values.street){
        errors.street = 'pidoras'
    }
    if(!values.house){
        errors.house = 'pidoras'
    }
    if(!values.flat){
        errors.flat = 'pidoras'
    }

    return errors
}

const Oformlenie = ({close, setNext, formData, submitHandler}) => {

    const formik = useFormik({
        initialValues: {
            name: '',
            phone: '',
            date: '',
            time: '',
            city: '',
            street: '',
            house: '',
            flat: '',
            comment: '',
            online: false
        },
        initialErrors: {
            name: '1',
            phone: '1',
            date: '1',
            time: '1'
        },
        validate,
        onSubmit: values => {
            submitHandler({...formData, ...values})
            setNext()
        },

    })

    const nextHandler = () => {
        // if(index === 0 && (formik.errors.name || formik.errors.phone || formik.errors.date || formik.errors.time)){
        //     return
        // }
        if (pageIndex < forms.length - 1) {
            setPageIndex(state => state + 1)
        }
    }

    const prevHandler = () => {
        if (pageIndex > 0) {
            setPageIndex(state => state - 1)
        }
    }

    const [pageIndex, setPageIndex] = useState(0)

    const forms = [
        () => (
            <div className={classes.container_content_form}>
                <div className={classes.container_content_form_control} className={clsx(formik.errors.name && classes.error)}>
                    <input
                        className={clsx(formik.errors.name && classes.error)}
                        id='name'
                        name='name'
                        type='text'
                        placeholder='фио'
                        onChange={formik.handleChange}
                        value={formik.values.name}
                    />
                </div>
                {/*<MaskedInput*/}
                <div className={classes.container_content_form_control} className={clsx(formik.errors.phone && classes.error)}>
                    <InputMask
                        id='phone'
                        name='phone'
                        type='text'
                        placeholder='телефон'
                        mask={'+7(999)999-99-99'}
                        onChange={formik.handleChange}
                        value={formik.values.phone}
                    />
                </div>
                <div className={classes.container_content_form_control} className={clsx(formik.errors.date && classes.error)}>
                    <InputMask
                        id='date'
                        name='date'
                        type='text'
                        placeholder='дата'
                        mask={'99.99.9999'}
                        onChange={formik.handleChange}
                        value={formik.values.date}
                    />
                </div>
                <div className={classes.container_content_form_control} className={clsx(formik.errors.time && classes.error)}>
                    <InputMask
                        id='time'
                        name='time'
                        type='text'
                        placeholder='время'
                        mask={'99:99'}
                        onChange={formik.handleChange}
                        value={formik.values.time}
                    />
                </div>
            </div>
        ),
        () => (
            <div className={classes.container_content_form}>
                <div className={classes.container_content_form_control} className={clsx(formik.errors.city && classes.error)}>
                    <select
                        id='city'
                        name='city'
                        placeholder="город"
                        onChange={formik.handleChange}
                        value={formik.values.city}
                    >
                        <option selected disabled value='none'>Выберите город</option>
                        <option value='moscow'>Москва</option>
                        <option value='SPB'>Санкт-Петербург</option>
                    </select>
                </div>
                <div className={classes.container_content_form_control} className={clsx(formik.errors.street && classes.error)}>
                    <input
                        id='street'
                        name='street'
                        type='text'
                        placeholder='улица'
                        onChange={formik.handleChange}
                        value={formik.values.street}
                    />
                </div>
                <div className={classes.container_content_form_control} className={clsx(formik.errors.house && classes.error)}>
                    <input
                        id='house'
                        name='house'
                        type='text'
                        placeholder='дом'
                        onChange={formik.handleChange}
                        value={formik.values.house}
                    />
                </div>
                <div className={classes.container_content_form_control} className={clsx(formik.errors.flat && classes.error)}>
                    <input
                        id='flat'
                        name='flat'
                        type='text'
                        placeholder='квартира'
                        onChange={formik.handleChange}
                        value={formik.values.flat}
                    />
                </div>
            </div>
        ),
        () => (
            <div className={classes.container_content_form}>
                <div className={classes.container_content_form_text}>
                    <div className={classes.container_content_form_text_left}>
                        <p>сумма заказа:</p>
                        <p>адрес:</p>
                        <p>телефон:</p>
                    </div>
                    <div className={classes.container_content_form_text_right}>
                        <p>{formData?.price} ₽</p>
                        <p>ул. {`${formik.values.street}, д. ${formik.values.house}, кв. ${formik.values.flat}`}</p>
                        <p>{formik.values.phone}</p>
                    </div>
                </div>
                <div className={classes.container_content_form_control
                }>
                    <textarea
                        id='comment'
                        name='comment'
                        placeholder='комментарий'
                        onChange={formik.handleChange}
                        value={formik.values.comment}
                    />
                </div>
                <div className={classes.container_content_form_buttons}>
                    <button type='submit' onClick={() => formik.setValues({...formik.values, online: true})}>Заказ
                        онлайн
                    </button>
                    <button type='submit' onClick={() => formik.setValues({...formik.values, online: false})}>Заказ со
                        звонком
                    </button>
                </div>
            </div>
        )
    ]

    return (
        <div className={classes.container}>
            <div className={classes.container_close} onClick={close}>
                <Close/>
            </div>
            <h1>Информация о заказе</h1>
            <form className={classes.container_content} onSubmit={formik.handleSubmit}>
                <div className={clsx(classes.container_content_arrow, pageIndex === 0 ? classes.hidden : null)}
                     onClick={() => prevHandler()}>
                    <Prev/>
                </div>
                {forms[pageIndex]()}
                <div className={clsx(classes.container_content_arrow,
                    (pageIndex === 0 && (formik.errors.name || formik.errors.phone || formik.errors.date || formik.errors.time)) ||
                    (pageIndex === 1 && (formik.errors.city || formik.errors.street || formik.errors.house || formik.errors.flat)) ||
                        pageIndex === 2
                        ? classes.hidden : null)}
                     onClick={() => nextHandler()}>
                    <Next/>
                </div>
            </form>
            <div className={clsx(classes.container_pagination, pageIndex === 0 && classes.container_pagination_first, pageIndex === 1 && classes.container_pagination_second, pageIndex === 2 && classes.container_pagination_third)}></div>
        </div>
    )
}

export default Oformlenie
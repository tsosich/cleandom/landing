import React, {useEffect, useState} from "react";
import classes from './index.module.sass'
import Option from "./Option";
import FormOrder from "./FormOrder";
import Form from "./Form";
import Oformlenie from "./Oformlenie";
import {ReactComponent as Close} from "../../assets/close.svg";

const options = [
    {
        title: 'Генеральная',
        text: 'Гарантированно отличный результат. Мы вернем вам забытый комфорт первозданно чистой квартиры',
        price: 3000
    },
    {
        title: 'Поддерживающая',
        text: 'Скажите рутине — стоп! Наша компания может вам не только сэкономить время и силы, но и подарить ощущение уюта в вашем доме.',
        price: 3000
    },
    {
        title: 'После ремонта',
        text: 'Мы готовы помочь в борьбе с неприятными последствиями ремонта : позаботимся о том, чтобы в комнатах стало чисто и комфортно.',
        price: 3000
    },
]

const WhatWeCan = () => {

    // if (isSubmit) {

    // }

    const [formData, setFormData] = useState({})
    const [stage, setStage] = useState(0)

    const submitHandler = (data) => {
        console.log(data)
    }

    const closeHandler = () => {
        setStage(0)
        setFormData({})
    }

    const setValue = (key, value) => {
        setFormData(prevState => {
            return {
                ...prevState,
                [key]: value
            }
        })
    }

    const nextStage = () => {
        if (stage + 1 > 4) {
            setStage(0)
        } else {
            setStage(stage => stage + 1)
        }
    }


    const firstStage = () => {
        return (
            <div className={classes.options}>
                {options.map(option => (
                    <Option
                        title={option.title}
                        text={option.text}
                        price={option.price}
                        setValue={setValue}
                        setNext={nextStage}
                    />
                ))}
            </div>
        )
    }

    console.log(formData)

    const secondStage = () => {
        return (
            <FormOrder close={closeHandler} title={formData.type} price={3000} setValue={setValue} setNext={nextStage}/>
        )
    }

    const thirdStage = () => {
        return (
            <Oformlenie close={closeHandler} setNext={nextStage} formData={formData} submitHandler={submitHandler}/>
        )
    }

    const fourthStage = () => {
        return (
            <div className={classes.container_zaglushka}>
                <div className={classes.container_zaglushka_close} onClick={closeHandler}>
                    <Close/>
                </div>
                <h1>ВЫ ВОСХИТИТЕЛЬНЫ!</h1>
                <p>Cпасибо, что воспользовались нашими услугами
                    ждите звонка от нашего сотрудника!</p>

            </div>
        )
    }

    const chooseStage = () => {
        switch (stage) {
            case 0:
                return firstStage()
            case 1:
                return secondStage()
            case 2:
                return thirdStage()
            case 3:
                return fourthStage()
        }
    }

    return (
        <div className={classes.container} id={'services'}>
            <h1>ЧТО МЫ МОЖЕМ?</h1>
            <div className={classes.container_content}>
                {chooseStage()}
            </div>
        </div>
    )
}

export default WhatWeCan
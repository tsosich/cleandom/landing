import React from "react";
import GreenRectangle from "../../hoc/GreenRectangle";
import classes from './index.module.sass'
import {ReactComponent as Security} from "../../assets/advantages/security.svg";
import {ReactComponent as Eco} from "../../assets/advantages/eco.svg";
import {ReactComponent as Sto} from "../../assets/advantages/sto.svg";
import {ReactComponent as Rouble} from "../../assets/advantages/rouble.svg";
import {ReactComponent as Time} from "../../assets/advantages/time.svg";
import {ReactComponent as Positive} from "../../assets/advantages/positive.svg";

const advantages = [
    // {icon:()=><Security/>, text:'мы отвечаем за сохранность имущества'},
    // {icon:()=><Eco/>, text:'экологически чистые средства очистки'},
    // {icon:()=><Sto/>, text:'мы гарантируем 100% качество'},
    // {icon:()=><Rouble/>, text:'оптимальное соотношение цены и качества'},
    // {icon:()=><Time/>, text:'мы соблюдаем оговоренные сроки и работаем круглосуточно'},
    // {icon:()=><Positive/>, text:'мы дарим позитивное настроение'},
    () => <Security/>,
    () => <Eco/>,
    () => <Sto/>,
    () => <Rouble/>,
    () => <Time/>,
    () => <Positive/>,
]

const WhyUs = () => {

    return (
        <GreenRectangle style={{marginTop: '160px'}} id={'advantages'}>
            <div className={classes.container}>
                <h1>ПОЧЕМУ МЫ?</h1>
                <div className={classes.container_advantages}>
                    {advantages.map(item =>
                        // <div className={classes.container_advantages_item}>
                        //     {item.icon()}
                        //     <p>{item.text}</p>
                        // </div>
                        item()
                    )}
                </div>
            </div>
        </GreenRectangle>
    )
}

export default WhyUs
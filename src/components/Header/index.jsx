import React from "react"
import {ReactComponent as Logo} from "../../assets/Logo.svg"
import {Link} from "react-scroll";
import classes from './index.module.sass'

const items = [
    {title:'о нас', link:'about'},
    {title:'услуги', link:'services'},
    {title:'преимущества', link:'advantages'},
    {title:'отзывы', link:'feedback'},
    {title:'контакты', link:'contacts'},
]

const Header = () => {

    return (
        <div className={classes.container}>
            <div className={classes.container_logo}>
                <Logo/>
            </div>
            <div className={classes.container_menu}>
                {items.map((item, index) => (
                    <Link
                        to={item.link}
                        className={classes.container_menu_item}
                        key={index}
                        activeClass={classes.container_menu_item_active}
                        offset={-70}
                        duration={500}
                        smooth={true}
                        spy={true}
                    >
                        <p>{item.title.toUpperCase()}</p>
                    </Link>
                ))}
            </div>
        </div>
    )
}

export default Header
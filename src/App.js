import './App.css';
import Layout from "./hoc/Layout";
import WhoWeAre from "./components/WhoWeAre";
import WhatWeCan from "./components/WhatWeCan";
import WhyUs from "./components/WhyUs";
import Done from "./components/Done";

function App() {
    return (
        <Layout>
            <WhoWeAre/>
            <WhatWeCan/>
            <WhyUs/>
            <Done/>
        </Layout>
    );
}

export default App;

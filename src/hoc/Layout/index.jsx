import React from "react";
import Header from "../../components/Header";
import classes from './index.module.sass'
import Footer from "../../components/Footer";

const Layout = ({children}) => {

    return (
        <div className={classes.container}>
            <Header/>
            {children}
            <Footer/>
        </div>
    )
}

export default Layout
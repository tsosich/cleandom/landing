import React from "react";

const GreenRectangle = ({style, children, id}) => (
    <div
        id={id}
        style={{
        ...style,
        background: "url('/icons/ass.png') no-repeat",
        padding: '200px 0',
        height: '850px'
    }}>
        {children}
    </div>
)

export default GreenRectangle
